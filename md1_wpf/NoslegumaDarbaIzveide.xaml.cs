﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

using md1.BibTex;


namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for MagistraDarbaIzveide.xaml
    /// Skatīt klasi md1_wpf.GramataIzveide, lai iegūtu detalizētāku aprakstu
    /// </summary>
    public partial class NoslegumaDarbaIzveide : Window
    {
        private Program mProgram;
        private bool editing = false;
        private int index = -1;
        private NoslegumaDarbs initialWork;

        public NoslegumaDarbaIzveide(Program p)
        {
            mProgram = p;
            InitializeComponent();
        }

        public NoslegumaDarbaIzveide(Program p, BibliografisksVienums g, int index)
        {
            initialWork = (NoslegumaDarbs)g;
            this.index = index;
            mProgram = p;
            editing = true;


            InitializeComponent();

            this.Title = "Noslēguma darba rediģēšana";
            ((Button)this.FindName("CreateBtn")).Content = "Labot";
            FillInitialValues();

        }

        private void FillInitialValues()
        {
            TextBox author = (TextBox)this.FindName("author");
            TextBox title = (TextBox)this.FindName("title");
            TextBox year = (TextBox)this.FindName("year");
            TextBox school = (TextBox)this.FindName("school");

            author.Text = initialWork.GetAuthorsSeperatedBy(",", " ");
            title.Text = initialWork.nosaukums;
            year.Text = initialWork.gads.ToString();
            school.Text = initialWork.IzglitibasIestade;

            ComboBoxItem selectedItem;
            ComboBox typeBox = (ComboBox)this.FindName("thesisType");

            if(initialWork.DarbaVeids == NoslegumaDarbaTips.MAGISTRA_DARBS)
            {
                selectedItem = (ComboBoxItem)this.FindName("mastersthesis");
            }
            else
            {
                selectedItem = (ComboBoxItem)this.FindName("phdthesis");
            }

            typeBox.SelectedItem = selectedItem;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Create(object sender, RoutedEventArgs e)
        {
            List<TextBox> inputs = new List<TextBox>();
            TextBox author = (TextBox)this.FindName("author");
            TextBox title = (TextBox)this.FindName("title");
            TextBox year = (TextBox)this.FindName("year");
            TextBox school = (TextBox)this.FindName("school");
            inputs.Add(title);
            inputs.Add(year);
            inputs.Add(author);
            inputs.Add(school);

            bool[] validated = Misc.NotEmptyInputs(inputs);
            bool canCreate = true;
            for (int i = 0; i < validated.Length; i++)
            {
                if (!(validated[i]))
                {
                    inputs[i].BorderBrush = new SolidColorBrush(Colors.Red);
                    canCreate = false;
                }
                else
                {
                    inputs[i].BorderBrush = new SolidColorBrush(Colors.Green);
                }
            }

            string authorVal = author.Text.Trim();
            if (authorVal == "" || authorVal.IndexOf(' ') == -1 || authorVal.Split(' ').Length != 2)
            {
                author.BorderBrush = new SolidColorBrush(Colors.Red);
                canCreate = false;
            }

            string yearVal = year.Text.Trim();
            if (yearVal == "" || int.Parse(yearVal) <= 1800 || int.Parse(yearVal) > DateTime.Now.Year + 1)
            {
                year.BorderBrush = new SolidColorBrush(Colors.Red);
                canCreate = false;
            }

            if(canCreate)
            {
                ComboBox typeBox = (ComboBox)this.FindName("thesisType");
                ComboBoxItem selectedElement = (ComboBoxItem)typeBox.SelectedItem;
                string typeKey = selectedElement.Name;

                EntryTypes type = TypeValueToEntryType.literalToEnum[typeKey];
                NoslegumaDarbaTips tips;
                if(type == EntryTypes.PHDTHESIS)
                {
                    tips = NoslegumaDarbaTips.DOKTORA_DISERTACIJA;
                }
                else
                {
                    tips = NoslegumaDarbaTips.MAGISTRA_DARBS;
                }

                NoslegumaDarbs noslegumaDarbs = new NoslegumaDarbs(author.Text.Split(' ')[0], author.Text.Split(' ')[1], title.Text, tips, school.Text, int.Parse(year.Text));

                if (editing)
                {
                    mProgram.mNoslegumaDarbi[index] = noslegumaDarbs;
                }
                else
                {
                    mProgram.mNoslegumaDarbi.Add(noslegumaDarbs);
                }
                Cancel(new object(), new RoutedEventArgs());
            }

        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Window rI;

            if (editing)
            {
                rI = new VienumuSaraksts(mProgram);
            }
            else
            {
                rI = new VienumaIzveide(mProgram);
            }
            Misc.RedirectWindow((Window)this, rI);
        }
    }
}
