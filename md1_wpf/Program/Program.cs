﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using md1.BibTex;
using md1.Helpers;
using md1.BibTex.IO;

namespace md1_wpf
{
    /// <summary>
    /// Šī ir lietotnes iekšējā stāvokļa klase, kas satur visus WPF lietotnē reģistrētos vienumus.
    /// Klase ļauj piekļūt pie attiecīgo vienumu sarakstiem, realizē vienkāršas vienumu / to datu iegūšanas funkcijas, 
    /// kas balstās uz sarakstu indeksiem, rakstu veidiem.
    /// </summary>
    public class Program
    {
        public List<Gramata> mGramatas = new List<Gramata>();
        public List<NoslegumaDarbs> mNoslegumaDarbi = new List<NoslegumaDarbs>();
        public List<Raksts> mRaksti = new List<Raksts>();

        public Program()
        {
        
        }

        public bool isEmpty()
        {
            if(mGramatas.Count == mNoslegumaDarbi.Count && mGramatas.Count == mRaksti.Count && mGramatas.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string getEntryBibTeXMarkup(int index, EntryTypes type)
        {
            string output = "";
            if(type == EntryTypes.BOOK && mGramatas.Count - 1 >= index && index >= 0)
            {
                output = mGramatas[index].Izdrukat();
            }
            else if (type == EntryTypes.MASTERSTHESIS || type == EntryTypes.PHDTHESIS && mNoslegumaDarbi.Count - 1 >= index && index >= 0)
            {
                output = mNoslegumaDarbi[index].Izdrukat();
            }
            else if(type == EntryTypes.ARTICLE && mRaksti.Count - 1 >= index && index >= 0)
            {
                output = mRaksti[index].Izdrukat();
            }

            return output;
        }

        public BibliografisksVienums getAsBiblVienums(int index, EntryTypes entryType)
        {
            BibliografisksVienums output = null;
            if (entryType == EntryTypes.BOOK && mGramatas.Count - 1 >= index && index >= 0)
            {
                output = (BibliografisksVienums)mGramatas[index];
            }
            else if (entryType == EntryTypes.MASTERSTHESIS || entryType == EntryTypes.PHDTHESIS && mNoslegumaDarbi.Count - 1 >= index && index >= 0)
            {
                output = (BibliografisksVienums)mNoslegumaDarbi[index];
            }
            else if (entryType == EntryTypes.ARTICLE && mRaksti.Count - 1 >= index && index >= 0)
            {
                output = (BibliografisksVienums)mRaksti[index];
            }

            return output;

        }

        public string getAllEntryMarkups()
        {
            string markup = "";

            for (int i = 0; i < mGramatas.Count; i++)
            {
                markup += mGramatas[i].Izdrukat() + "\n";
            }

            for (int i = 0; i < mNoslegumaDarbi.Count; i++)
            {
                markup += mNoslegumaDarbi[i].Izdrukat() + "\n";
            }

            for (int i = 0; i < mRaksti.Count; i++)
            {
                markup += mRaksti[i].Izdrukat() + "\n";
            }

            return markup;
        }

        private void tryParseSetLarger(string possibleId, ref uint output)
        {
            string key = possibleId.Replace("key", "");
            uint possible;
            if (uint.TryParse(key, out possible))
            {
                if (output < possible)
                {
                    output = possible;
                }
            }


        }

        public uint findLargestKeyId()
        {
            uint output = 0;
            for (int i = 0; i < mRaksti.Count; i++)
            {
                tryParseSetLarger(mRaksti[i].Atslega.Trim(), ref output);
            }

            for (int i = 0; i < mNoslegumaDarbi.Count; i++)
            {
                tryParseSetLarger(mNoslegumaDarbi[i].Atslega.Trim(), ref output);
            }

            for (int i = 0; i < mGramatas.Count; i++)
            {
                tryParseSetLarger(mGramatas[i].Atslega.Trim(), ref output);
            }

            return output + 1;
        }

        public void clearAllData()
        {
            mGramatas = new List<Gramata>();
            mNoslegumaDarbi = new List<NoslegumaDarbs>();
            mRaksti = new List<Raksts>();
        }

    }
}
