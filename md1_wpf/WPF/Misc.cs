﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace md1_wpf
{
    /// <summary>
    /// Palīgklase, kas tiek izmantota UI puses realizācijā. Klase ļauj nomainīt aktīvo logu uz citu, saglabājot iepriekšējā loga 
    /// pozīciju / izmēru, veic primitīvu lauku validāciju, teksta samazināšanu. 
    /// </summary>
    static class Misc
    {
        public static void DuplicateWindowOrientation(Window oldWindow, Window newWindow)
        {
            newWindow.Width = oldWindow.Width;
            newWindow.Height = oldWindow.Height;

            newWindow.Left = oldWindow.Left;
            newWindow.Top = oldWindow.Top;
        }

        public static void RedirectWindow(Window oldWindow, Window newWindow)
        {
            Misc.DuplicateWindowOrientation(oldWindow, newWindow);
            newWindow.Show();
            oldWindow.Close();
        }

        public static bool[] NotEmptyInputs(List<TextBox> textBoxs)
        {
            bool[] validated = new bool[textBoxs.Count];
            for (int i = 0; i < textBoxs.Count; i++)
            {
                string value = textBoxs[i].Text;
                if (value.Trim() == "")
                {
                    validated[i] = false;
                }
                else
                {
                    validated[i] = true;
                }
            }

            return validated;
        }


        public static string TruncateIfTooLong(string str, int maxLen)
        {
            if(str.Length <= maxLen)
            {
                return str;
            }
            else
            {
                return str.Substring(0, maxLen) + " ...";
            }

        }

    }
}
