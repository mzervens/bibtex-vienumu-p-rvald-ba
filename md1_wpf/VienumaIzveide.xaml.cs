﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using md1.BibTex;

namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for RakstaIzveide.xaml
    /// Vienuma izveides pirmsapstrādes klase, klase nodrošina pāreju uz konkrēta vienuma izveides logu 
    /// (md1_wpf.GramataIzveide, md1_wpf.NoslegumaDarbaIzveide, md1_wpf.RakstaIzveide)
    /// </summary>
    public partial class VienumaIzveide : Window
    {
        private EntryTypes selectedType = EntryTypes.BOOK;
        private Program mProgram;

        public VienumaIzveide(Program p)
        {
            mProgram = p;
            InitializeComponent();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)box.SelectedItem;

            if (TypeValueToEntryType.literalToEnum.ContainsKey(selectedItem.Name))
            {
                selectedType = TypeValueToEntryType.literalToEnum[selectedItem.Name];
            }
        }

        private void Create(object sender, RoutedEventArgs e)
        {
            Window nextWindow;
            if (selectedType == EntryTypes.MASTERSTHESIS || selectedType == EntryTypes.PHDTHESIS)
            {
                nextWindow = new NoslegumaDarbaIzveide(mProgram);
            }
            else if (selectedType == EntryTypes.ARTICLE)
            {
                nextWindow = new RakstaIzveide(mProgram);
            }
            else
            {
                nextWindow = new GramataIzveide(mProgram);
            }

            Misc.DuplicateWindowOrientation((Window)this, nextWindow);

            nextWindow.Show();
            this.Close();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Window mW = new MainWindow(mProgram);
            Misc.RedirectWindow((Window)this, mW);
        }
    }
}
