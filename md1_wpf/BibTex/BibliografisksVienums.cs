﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using md1.Helpers;

namespace md1.BibTex
{
    /// <summary>
    /// Vispārīgā klase, kas nodrošina BibTeX vienuma izveides pamatfunkcionalitāti
    /// Galvenā funkcionalitāte nodrošina autora(-u) pievienošanu un to vārdu uzvārdu iegūšanu (klase nodrošina arī viena autora ierobežošanu)
    /// Papildus tiek realizēta objekta inicializācija no vispārīgas entītijas objekta (imports), šajā gadījumā tiek inicializēti bērnu klašu kopīgie atribūti
    /// </summary>
    public abstract class BibliografisksVienums
    {
        protected string mkey = "key" + KeyGenerator.UseKey().ToString();
        private string mNosaukums = "";
        private int mGads = -1;
        private Date mIzveidosanasDatums = new Date();
        private List<Autors> mAutori = new List<Autors>();
        protected bool mSingleAuthor = false;

        public BibliografisksVienums()
        {
        }

        public BibliografisksVienums(Date dateCreated)
        {
            SetCreationDate(dateCreated);
        }
        protected void SetCreationDate(Date dateCreated)
        {
            mIzveidosanasDatums = dateCreated;
        }
        

        public void AddAuthor(Autors a)
        {
            if(mSingleAuthor && mAutori.Count >= 1)
            {
                ResetAuthors();
                mAutori.Add(a);
                return;
            }

            mAutori.Add(a);
        }

        public void SetAuthorsFromString(string authorsSepByComma)
        {
            if(mSingleAuthor && authorsSepByComma.IndexOf(',') != -1)
            {
                return;
            }

            ResetAuthors();

            string[] authors = authorsSepByComma.Trim().Split(',');
            List<Autors> parsedAuthors = new List<Autors>();

            for (int i = 0; i < authors.Length; i++)
            {
                authors[i] = authors[i].Trim();
                Autors a = new Autors(authors[i]);
                parsedAuthors.Add(a);
            }

            this.SetAuthors(parsedAuthors);
        }

        public void SetAuthors(List<Autors> authors)
        {
            if(mSingleAuthor && authors.Count > 1)
            {
                ResetAuthors();
                mAutori.Add(authors[0]);
                return;
            }

            mAutori = authors;
        }

        public void ResetAuthors()
        {
            mAutori = new List<Autors>();
        }

        public string GetAuthorsSeperatedBy(string sepBtwnAuthors, string sepBtwnNameSurname)
        {
            string output = "";
            bool enteredCycle = false;

            foreach(Autors a in mAutori)
            {
                enteredCycle = true;
                output += a.GetValuesSeperatedBy(sepBtwnNameSurname) + sepBtwnAuthors + " ";
            }

            if (enteredCycle)
            {
                output = output.Substring(0, output.LastIndexOf(sepBtwnAuthors));
            }

            return output;
        }

        public virtual string Izdrukat()
        {
            return "print class as BibTeX";

        }

        public virtual bool fromImportEntity(BibTex.IO.ImportEntity entity)
        {
            bool valid = true;
            if (entity.mEntries.ContainsKey("author"))
            {
                string authors = entity.mEntries["author"];
                SetAuthorsFromString(authors);

                if(mSingleAuthor && authors.IndexOf(',') != -1)
                {
                    valid = false;
                }
            }
            else
            {
                valid = false;
            }

            if (entity.mEntries.ContainsKey("title"))
            {
                string title = entity.mEntries["title"];
                this.nosaukums = title;
            }
            else
            {
                valid = false;
            }

            if (entity.mEntries.ContainsKey("year"))
            {
                string yearStr = entity.mEntries["year"];
                int year;

                if (!(int.TryParse(yearStr, out year)))
                {
                    year = -1;
                    valid = false;
                }
                else
                {
                    this.gads = year;
                }
            }
            else
            {
                valid = false;
            }

            string dateCreated = "";
            DateTime dt = DateTime.Now;
            if (entity.mEntries.TryGetValue("timestamp", out dateCreated))
            {
                string[] dateParts = dateCreated.Split('.');
                if (dateParts.Length == 3)
                {
                    dt = new DateTime(int.Parse(dateParts[0]), int.Parse(dateParts[1]), int.Parse(dateParts[2]));
                }
            }

            Date created = new Date(dt);
            SetCreationDate(created);

            return valid;
        }


        public string nosaukums
        {
            get
            {
                return mNosaukums;
            }
            set
            {
                mNosaukums = value;
            }
        }

        public int gads
        {
            get
            {
                if (mGads == -1)
                {
                    throw new InvalidOperationException("Accessing value from uninitialized" +
                                                        "variable 'mGads' in class 'BibliografisksVienums'");
                }

                return mGads;
            }
            set
            {
                int nextYear = DateTime.Now.Year + 1;
                if (value > 1800 && value <= nextYear)
                {
                    mGads = value;
                }

            }
        }

        public Date izveidosanasDatums
        {
            get
            {
                return mIzveidosanasDatums;
            }
        }


    }
}
