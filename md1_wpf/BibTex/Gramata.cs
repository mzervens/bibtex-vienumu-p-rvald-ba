﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex
{
    /// Uzdevumā aprakstītā grāmatas klase
    /// Bez uzdevumā definētā, tiek realizēta metode, kas ļauj inicializēt objektu no vispārīgas entītijas(faila imports)
    public class Gramata : BibliografisksVienums
    {

        private string mIzdevejs = "";
        private string mIzdevejaAdrese = "";


        public Gramata()
        {
        }

        public Gramata(string autori, string nosaukums, string izdevnieciba,
                       int gads, string iIzdevniecibasAdrese = "")
        {

            this.nosaukums = nosaukums.Trim();
            this.gads = gads;

            mIzdevejs = izdevnieciba.Trim();
            mIzdevejaAdrese = iIzdevniecibasAdrese.Trim();


            SetAuthorsFromString(autori.Trim());

        }

        public override bool fromImportEntity(IO.ImportEntity entity)
        {
            bool valid = true;
            mkey = entity.mKey != "" ? entity.mKey : mkey;
            mIzdevejaAdrese = "";

            valid = base.fromImportEntity(entity);

            if (entity.mEntries.ContainsKey("publisher"))
            {
                string publisher = entity.mEntries["publisher"];
                mIzdevejs = publisher;
            }
            else
            {
                valid = false;
            }


            if(!(entity.mEntries.TryGetValue("address", out mIzdevejaAdrese)))
            {
                mIzdevejaAdrese = "";
            }

            return valid;
        }

        public override string Izdrukat()
        {
            string output = "";

            output += "@BOOK{" + mkey + ",\n  ";
            output += "title = {" + nosaukums + "},\n  ";
            output += "publisher = {" + mIzdevejs + "},\n  ";
            output += "year = {" + gads.ToString() + "},\n  ";
            output += "author = {" + GetAuthorsSeperatedBy(",", " ") + "},\n  ";

            if(mIzdevejaAdrese != "")
            {
                output += "address = {" + mIzdevejaAdrese + "},\n  ";
            }

            output += "owner = {User},\n  ";
            output += "timestamp = {" + izveidosanasDatums.Value.ToString("yyyy.MM.dd") + "}";

            output += "\n}\n";

            return output;
        }


        public string Izdevejs
        {
            get
            {
                return mIzdevejs;
            }
        }

        public string IzdevejaAdrese
        {
            get
            {
                return mIzdevejaAdrese;
            }
        }

        public string Atslega
        {
            get
            {
                return mkey;
            }
            set
            {
                mkey = value;
            }
        }
    }
}
