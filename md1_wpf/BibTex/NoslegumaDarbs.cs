﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex
{
    /// <summary>
    /// Uzdevumā aprakstītā noslēguma darba klase
    /// Bez uzdevumā definētā, tiek realizēta metode, kas ļauj inicializēt objektu no vispārīgas entītijas(faila imports)
    /// </summary>
    public class NoslegumaDarbs : BibliografisksVienums
    {
        private NoslegumaDarbaTips mDarbaVeids = NoslegumaDarbaTips.MAGISTRA_DARBS;
        private string mIzglitibasIestade = "";

        public NoslegumaDarbs()
        {
            mSingleAuthor = true;
        }

        public NoslegumaDarbs(string autoraVards, string autoraUzvards, string nosaukums,
                              NoslegumaDarbaTips darbaVeids, string izglitibasIestadesNos, int gads) : this()
        {

            Autors a = new Autors(autoraVards.Trim(), autoraUzvards.Trim());
            AddAuthor(a);

            this.nosaukums = nosaukums.Trim();
            mDarbaVeids = darbaVeids;
            mIzglitibasIestade = izglitibasIestadesNos.Trim();
            this.gads = gads;
        }

        public override bool fromImportEntity(IO.ImportEntity entity)
        {
            bool valid = true;
            mkey = entity.mKey != "" ? entity.mKey : mkey;

            valid = base.fromImportEntity(entity);

            if (entity.mEntries.ContainsKey("school"))
            {
                string school = entity.mEntries["school"];
                mIzglitibasIestade = school;
            }
            else
            {
                valid = false;
            }

            if(entity.mTypeId == EntryTypes.MASTERSTHESIS)
            {
                mDarbaVeids = NoslegumaDarbaTips.MAGISTRA_DARBS;
            }
            else if(entity.mTypeId == EntryTypes.PHDTHESIS)
            {
                mDarbaVeids = NoslegumaDarbaTips.DOKTORA_DISERTACIJA;
            }
            else
            {
                valid = false;
            }

            return valid;
        }

        public override string Izdrukat()
        {
            string output = "";

            if(mDarbaVeids == NoslegumaDarbaTips.DOKTORA_DISERTACIJA)
            {
                output += "@PHDTHESIS{" + mkey + ",\n  ";
            }
            else if (mDarbaVeids == NoslegumaDarbaTips.MAGISTRA_DARBS)
            {
                output += "@MASTERSTHESIS{" + mkey + ",\n  ";
            }

            output += "title = {" + nosaukums + "},\n  ";
            output += "year = {" + gads.ToString() + "},\n  ";
            output += "author = {" + GetAuthorsSeperatedBy(",", " ") + "},\n  ";
            output += "school = {" + mIzglitibasIestade + "},\n  ";



            output += "owner = {User},\n  ";
            output += "timestamp = {" + izveidosanasDatums.Value.ToString("yyyy.MM.dd") + "}";

            output += "\n}";

            return output;
        }

        public NoslegumaDarbaTips DarbaVeids
        {
            get
            {
                return mDarbaVeids;
            }
        }

        public string IzglitibasIestade
        {
            get
            {
                return mIzglitibasIestade;
            }
        }

        public string Atslega
        {
            get
            {
                return mkey;
            }
            set
            {
                mkey = value;
            }
        }


    }
}
