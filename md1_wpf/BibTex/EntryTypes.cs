﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex
{
    /// <summary>
    /// Pārskaitāmais tips, kas identificē lietotnē atbalstītos BibTeX vienumus
    /// </summary>
    public enum EntryTypes
    {
        BOOK,
        PHDTHESIS,
        MASTERSTHESIS,
        ARTICLE,
        UNKNOWN
    }
}
