﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex
{
    /// <summary>
    /// Vienkārša Autora klase, kas glabā autora vārdu un uzvārdu
    /// Radīsies problēmas ar autoriem, kam ir 'dīvaini' vārdi / uzvārdi (vairāk par 1 atstarpi)
    /// </summary>
    public class Autors
    {
        public string name;
        public string surname;

        public Autors()
        {
            name = surname = "";
        }

        public Autors(string name, string surname)
        {
            this.name = name;
            this.surname = surname;
        }

        public Autors(string nameSurnameSepBySpace)
        {
            string[] parts = nameSurnameSepBySpace.Split(' ');
            if(parts.Length != 2)
            {
                throw new ArgumentException("Invalid authors name and surname supplied" +
                                            "for the constructor in class 'Autors'! \n '" + nameSurnameSepBySpace + "'" );
            }

            name = parts[0];
            surname = parts[1];

        }

        public string GetValuesSeperatedBy(string sep)
        {
            return name + sep + surname;
        }


    }
}
