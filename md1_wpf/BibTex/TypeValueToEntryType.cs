﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex
{
    /// <summary>
    /// Klase nodrošina literālo vienumu tipu vērtību konvertāciju uz pārskaitāmo tipu
    /// Izmanto, piemēram, iekš UI, lai no marķējumu atslēgu atribūtiem identificētu vienumus
    /// </summary>
    public static class TypeValueToEntryType
    {
        public static Dictionary<string, EntryTypes> literalToEnum = 
                                            new Dictionary<string,EntryTypes>{
                                                                                {"book", EntryTypes.BOOK},
                                                                                {"article", EntryTypes.ARTICLE},
                                                                                {"mastersthesis", EntryTypes.MASTERSTHESIS},
                                                                                {"phdthesis", EntryTypes.PHDTHESIS}
        
                                                                             };
    }
}
