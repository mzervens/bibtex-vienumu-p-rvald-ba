﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex
{
    /// <summary>
    /// Uzdevumā prasītais pārskaitāmais tips, sakarīgāk būtu bijis prasīt kaut ko tādu, kas tika realizēts md1.BibTex.EntryTypes
    /// </summary>
    public enum NoslegumaDarbaTips
    {
        MAGISTRA_DARBS,
        DOKTORA_DISERTACIJA
    }
}
