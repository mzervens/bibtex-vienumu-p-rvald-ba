﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex
{
    /// <summary>
    /// Raksta klase, kas ļauj veidot vienkāršotus BibTeX article vienumus
    /// </summary>
    public class Raksts : BibliografisksVienums
    {
        public string mVolume = "";
        public string mJournal = "";

        public Raksts()
        {

        }

        public Raksts(string authors, string title, int year, string journal, string volume)
        {
            this.nosaukums = title.Trim();
            this.gads = year;
            mJournal = journal.Trim();
            mVolume = volume.Trim();

            this.SetAuthorsFromString(authors.Trim());

        }

        public override bool fromImportEntity(IO.ImportEntity entity)
        {
            bool valid = true;
            mkey = entity.mKey != "" ? entity.mKey : mkey;
            mVolume = "";
            mJournal = "";

            valid = base.fromImportEntity(entity);

            if (entity.mEntries.ContainsKey("journal"))
            {
                mJournal = entity.mEntries["journal"];
            }
            else
            {
                valid = false;
            }

            if (entity.mEntries.ContainsKey("volume"))
            {
                mVolume = entity.mEntries["volume"];
            }
            else
            {
                valid = false;
            }


            return valid;
        }

        public override string Izdrukat()
        {
            string output = "";

            output += "@ARTICLE{" + mkey + ",\n  ";
            output += "title = {" + nosaukums + "},\n  ";
            output += "journal = {" + mJournal + "},\n  ";
            output += "year = {" + gads.ToString() + "},\n  ";
            output += "author = {" + GetAuthorsSeperatedBy(",", " ") + "},\n  ";
            output += "volume = {" + mVolume + "},\n  ";
            

            output += "owner = {User},\n  ";
            output += "timestamp = {" + izveidosanasDatums.Value.ToString("yyyy.MM.dd") + "}";

            output += "\n}\n";

            return output;
        }


        public string Atslega
        {
            get
            {
                return mkey;
            }
            set
            {
                mkey = value;
            }
        }
    }
}
