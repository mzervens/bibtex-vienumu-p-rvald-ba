﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// Galvenais UI logs, logs ļauj lietotājam pārvietoties no vienas lietotnes funkcionalitātes uz citu.
    /// Funkcionalitātes klases ir šādas:
    /// Imports / Eksports - md1_wpf.EksportsImports
    /// Vienumu saraksts - md1_wpf.VienumuSaraksts
    /// Vienuma izveide - md1_wpf.VienumaIzveide
    /// </summary>
    public partial class MainWindow : Window
    {
        private Program mProgram;

        public MainWindow()
        {
            InitializeComponent();
            mProgram = new Program();
        }

        public MainWindow(Program p)
        {
            InitializeComponent();
            mProgram = p;
        }

        private void CreateElement(object sender, RoutedEventArgs e)
        {
            Window rI = new VienumaIzveide(mProgram);
            Misc.RedirectWindow((Window)this, rI);
        }

        private void ViewElementList(object sender, RoutedEventArgs e)
        {
            Window vS = new VienumuSaraksts(mProgram);
            Misc.RedirectWindow((Window)this, vS);
        }

        private void ImportExport(object sender, RoutedEventArgs e)
        {
            Window eI = new EksportsImports(mProgram);
            Misc.RedirectWindow((Window)this, eI);
        }

        private void QuitApplication(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Tiešām beigt darbu?", "Apstiprinājums", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }
        



    }
}
