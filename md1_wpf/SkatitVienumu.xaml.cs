﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using md1.BibTex;

namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for SkatitVienumu.xaml
    /// Klase nodrošina lietotnē reģistrēta vienuma apskati.
    /// Lietotājam tiek parādīts vienuma BibTeX marķējums, lietotājs var rediģēt vienumu, ko realizē konkrētā 
    /// vienuma izveides klase (md1_wpf.GramataIzveide, md1_wpf.NoslegumaDarbaIzveide, md1_wpf.RakstaIzveide).
    /// Klasē nokļūst no klases md1_wpf.VienumuSaraksts, kad lietotājs izvēlas skatīt kādu no saraksta elementiem sīkāk.
    /// </summary>
    public partial class SkatitVienumu : Window
    {
        private Program mProgram;
        private EntryTypes entryType;
        private int index;
        private TextBlock markupLocation;

        public SkatitVienumu(Program p, int index, EntryTypes entryType)
        {
            mProgram = p;
            this.index = index;
            this.entryType = entryType;
            InitializeComponent();

            markupLocation = (TextBlock)this.FindName("Markup");

            ShowElementMarkup();
        }

        private void ShowElementMarkup()
        {
            string markup = mProgram.getEntryBibTeXMarkup(index, entryType);
            Run markupRun = new Run(markup.Replace("\n", Environment.NewLine));

            markupLocation.Inlines.Add(markupRun);
        }

        private void Edit(object sender, RoutedEventArgs e)
        {
            BibliografisksVienums vienums = mProgram.getAsBiblVienums(index, entryType);
            Window redirect;
            if(entryType == EntryTypes.BOOK)
            {
                redirect = new GramataIzveide(mProgram, vienums, index);
            }
            else if(entryType == EntryTypes.PHDTHESIS || entryType == EntryTypes.MASTERSTHESIS)
            {
                redirect = new NoslegumaDarbaIzveide(mProgram, vienums, index);
            }
            else if(entryType == EntryTypes.ARTICLE)
            {
                redirect = new RakstaIzveide(mProgram, vienums, index);
            }
            else
            {
                redirect = new MainWindow(mProgram);
            }


            Misc.RedirectWindow((Window)this, redirect);
        }

        private void BackToList(object sender, RoutedEventArgs e)
        {
            Window vS = new VienumuSaraksts(mProgram);
            Misc.RedirectWindow((Window)this, vS);
        }
    }
}
