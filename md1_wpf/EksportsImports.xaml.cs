﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.IO;
using md1.BibTex;
using md1.BibTex.IO;

namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for EksportsImports.xaml
    /// Klase nodrošina lietotnes iekšējā stāvokļa eksportu uz teksta failu un importu no teksta faila.
    /// Klase izmanto galvenokārt md1.BibTex.IO.Import un md1.BibTex.IO.Export klašu nodrošinātās funkcijas, lai realizētu šo funkcionalitāti
    /// </summary>
    public partial class EksportsImports : Window
    {
        private Program mProgram;
        private Label statusMsg;

        public EksportsImports(Program p)
        {
            mProgram = p;
            InitializeComponent();

            statusMsg = (Label)this.FindName("OperationStatuss");
        }

        private string OpenFileDialog()
        {
            string output = "";
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();


            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".bib";
            dlg.Filter = "BibTeX Files (*.bib)|*.bib|All Files (*.*)|*.*";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                output = dlg.FileName;
                Console.WriteLine(output);
            }

            return output;
        }

        private void Export(object sender, RoutedEventArgs e)
        {
            hideStatussMsg();
            string exportTo = OpenFileDialog();
            if(exportTo != "" && File.Exists(exportTo))
            {
                string exportData = mProgram.getAllEntryMarkups();
                md1.BibTex.IO.Export.ToTextFile(exportTo, exportData, true);
                showStatussMsg("Dati veiksmīgi eksportēti");
            }
            else
            {
                showStatussMsg("Neizdevās veikt datu eksportu");
            }

        }

        private void Import(object sender, RoutedEventArgs e)
        {
            hideStatussMsg();
            string importTo = OpenFileDialog();
            bool anySuccess = false;
            if(importTo != "" && File.Exists(importTo))
            {
                mProgram.clearAllData();
                List<ImportEntity> entities = md1.BibTex.IO.Import.FromTextFile(importTo);
                foreach (ImportEntity ie in entities)
                {
                    if (ie.mTypeId == EntryTypes.BOOK)
                    {
                        Gramata gr = new Gramata();
                        if (gr.fromImportEntity(ie))
                        {
                            anySuccess = true;
                            mProgram.mGramatas.Add(gr);
                        }
                        else
                        {
                            Console.WriteLine("Failure creating from import entity");
                        }

                    }
                    else if (ie.mTypeId == EntryTypes.PHDTHESIS || ie.mTypeId == EntryTypes.MASTERSTHESIS)
                    {
                        NoslegumaDarbs nd = new NoslegumaDarbs();
                        if (nd.fromImportEntity(ie))
                        {
                            anySuccess = true;
                            mProgram.mNoslegumaDarbi.Add(nd);
                        }
                        else
                        {
                            Console.WriteLine("failure creating from import entity");
                        }
                    }
                    else if (ie.mTypeId == EntryTypes.ARTICLE)
                    {
                        Raksts r = new Raksts();
                        if (r.fromImportEntity(ie))
                        {
                            anySuccess = true;
                            mProgram.mRaksti.Add(r);
                        }
                        else
                        {
                            Console.WriteLine("failure creating from import entity");
                        }

                    }

                }

                if(!(anySuccess))
                {
                    showStatussMsg("Neizdevās veikt datu importu");
                }
                else
                {
                    md1.Helpers.KeyGenerator.SetStartValue = mProgram.findLargestKeyId();
                    showStatussMsg("Datu imports veiksmīgs");
                }

            }
            else
            {
                showStatussMsg("Neizdevās veikt datu importu");
            }

        }

        private void hideStatussMsg()
        {
            statusMsg.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void showStatussMsg(string contents = "")
        {
            statusMsg.Visibility = System.Windows.Visibility.Visible;
            statusMsg.Content = contents;
        }

        private void Back(object sender, RoutedEventArgs e)
        {
            Window mW = new MainWindow(mProgram);
            Misc.RedirectWindow((Window)this, mW);
        }
    }
}
