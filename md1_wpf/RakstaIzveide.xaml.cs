﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

using md1.BibTex;

namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for RakstaIzveide.xaml
    /// Skatīt klasi md1_wpf.GramataIzveide, lai iegūtu detalizētāku aprakstu
    /// </summary>
    public partial class RakstaIzveide : Window
    {
        private Program mProgram;
        private Raksts initialArticle;
        private bool editing = false;
        private int index = -1;

        public RakstaIzveide(Program p)
        {
            mProgram = p;
            InitializeComponent();
        }

        public RakstaIzveide(Program p, BibliografisksVienums r, int index)
        {
            initialArticle = (Raksts)r;
            this.index = index;
            mProgram = p;
            editing = true;


            InitializeComponent();

            this.Title = "Raksta rediģēšana";
            ((Button)this.FindName("CreateBtn")).Content = "Labot";
            FillInitialValues();

        }


        private void FillInitialValues()
        {
            TextBox authors = (TextBox)this.FindName("authors");
            TextBox title = (TextBox)this.FindName("title");
            TextBox year = (TextBox)this.FindName("year");
            TextBox journal = (TextBox)this.FindName("journal");
            TextBox volume = (TextBox)this.FindName("volume");

            authors.Text = initialArticle.GetAuthorsSeperatedBy(",", " ");
            title.Text = initialArticle.nosaukums;
            year.Text = initialArticle.gads.ToString();
            journal.Text = initialArticle.mJournal;
            volume.Text = initialArticle.mVolume;
        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Create(object sender, RoutedEventArgs e)
        {
            List<TextBox> inputs = new List<TextBox>();
            TextBox authors = (TextBox)this.FindName("authors");
            TextBox title = (TextBox)this.FindName("title");
            TextBox year = (TextBox)this.FindName("year");
            TextBox journal = (TextBox)this.FindName("journal");
            TextBox volume = (TextBox)this.FindName("volume");
            inputs.Add(title);
            inputs.Add(year);
            inputs.Add(authors);
            inputs.Add(journal);
            inputs.Add(volume);

            bool[] validated = Misc.NotEmptyInputs(inputs);
            bool canCreate = true;
            for(int i = 0; i < validated.Length; i++)
            {
                if(!(validated[i]))
                {
                    inputs[i].BorderBrush = new SolidColorBrush(Colors.Red);
                    canCreate = false;
                }
                else
                {
                    inputs[i].BorderBrush = new SolidColorBrush(Colors.Green);
                }
            }

            string authorsVal = authors.Text.Trim();
            if(authorsVal == "" || authorsVal.IndexOf(' ') == -1)
            {
                authors.BorderBrush = new SolidColorBrush(Colors.Red);
                canCreate = false;
            }

            string yearVal = year.Text.Trim();
            if (yearVal == "" || int.Parse(yearVal) <= 1800 || int.Parse(yearVal) > DateTime.Now.Year + 1)
            {
                year.BorderBrush = new SolidColorBrush(Colors.Red);
                canCreate = false;
            }


            if(canCreate)
            {
                Raksts r = new Raksts(authors.Text, title.Text, int.Parse(year.Text), journal.Text, volume.Text);

                if (editing)
                {
                    r.Atslega = initialArticle.Atslega;
                    mProgram.mRaksti[index] = r;
                }
                else
                {
                    mProgram.mRaksti.Add(r);
                }
                Cancel(new object(), new RoutedEventArgs());
            }

            
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Window rI;

            if (editing)
            {
                rI = new VienumuSaraksts(mProgram);
            }
            else
            {
                rI = new VienumaIzveide(mProgram);
            }
            Misc.RedirectWindow((Window)this, rI);
        }
    
    }
}
