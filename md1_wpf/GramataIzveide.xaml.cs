﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

using md1.BibTex;

namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for GramataIzveide.xaml
    /// Klase nodrošina Grāmatas vienuma izveides un rediģēšanas funkcionalitāti.
    /// Lietotnē šajā klasē nonāk no klases md1_wpf.RakstaIzveide, ja tiek veidots jauns raksts, no md1_wpf.SkatitVienumu, ja
    /// tiek rediģēts jau esošs vienums.
    /// Klases kods ir strukturāli līdzīgs ar klasēm md1_wpf.NoslegumaDarbaIzveide un md1_wpf.RakstaIzveide, 
    /// šeit visticāmāk vajadzētu izmantot mantošanu / līdzīgā koda nodali citā klasē, laika trūkuma dēļ tas netika izdarīts.
    /// </summary>
    public partial class GramataIzveide : Window
    {
        
        private Program mProgram;

        private bool editing = false;
        private int index = -1;
        private Gramata initialBook;

        public GramataIzveide(Program p)
        {
            mProgram = p;
            InitializeComponent();
        }

        public GramataIzveide(Program p, BibliografisksVienums g, int index)
        {
            initialBook = (Gramata)g;
            this.index = index;
            mProgram = p;
            editing = true;


            InitializeComponent();

            this.Title = "Grāmatas rediģēšana";
            ((Button)this.FindName("CreateBtn")).Content = "Labot";
            FillInitialValues();

        }

        private void FillInitialValues()
        {
            TextBox authors = (TextBox)this.FindName("authors");
            TextBox title = (TextBox)this.FindName("title");
            TextBox year = (TextBox)this.FindName("year");
            TextBox publisher = (TextBox)this.FindName("publisher");
            TextBox publisherAddr = (TextBox)this.FindName("publisherAddress");

            authors.Text = initialBook.GetAuthorsSeperatedBy(",", " ");
            title.Text = initialBook.nosaukums;
            year.Text = initialBook.gads.ToString();
            publisher.Text = initialBook.Izdevejs;
            publisherAddr.Text = initialBook.IzdevejaAdrese;
        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Create(object sender, RoutedEventArgs e)
        {
            List<TextBox> inputs = new List<TextBox>();
            TextBox authors = (TextBox)this.FindName("authors");
            TextBox title = (TextBox)this.FindName("title");
            TextBox year = (TextBox)this.FindName("year");
            TextBox publisher = (TextBox)this.FindName("publisher");
            inputs.Add(title);
            inputs.Add(year);
            inputs.Add(authors);
            inputs.Add(publisher);

            bool[] validated = Misc.NotEmptyInputs(inputs);
            bool canCreate = true;
            for(int i = 0; i < validated.Length; i++)
            {
                if(!(validated[i]))
                {
                    inputs[i].BorderBrush = new SolidColorBrush(Colors.Red);
                    canCreate = false;
                }
                else
                {
                    inputs[i].BorderBrush = new SolidColorBrush(Colors.Green);
                }
            }

            string authorsVal = authors.Text.Trim();
            if(authorsVal == "" || authorsVal.IndexOf(' ') == -1)
            {
                authors.BorderBrush = new SolidColorBrush(Colors.Red);
                canCreate = false;
            }

            string yearVal = year.Text.Trim();
            if (yearVal == "" || int.Parse(yearVal) <= 1800 || int.Parse(yearVal) > DateTime.Now.Year + 1)
            {
                year.BorderBrush = new SolidColorBrush(Colors.Red);
                canCreate = false;
            }


            if(canCreate)
            {
                TextBox publisherAddress = (TextBox)this.FindName("publisherAddress");

                Gramata g = new Gramata(authors.Text, title.Text, publisher.Text, int.Parse(year.Text), publisherAddress.Text);

                if (editing)
                {
                    g.Atslega = initialBook.Atslega;
                    mProgram.mGramatas[index] = g;
                }
                else
                {
                    mProgram.mGramatas.Add(g);
                }
                Cancel(new object(), new RoutedEventArgs());
            }

            
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            Window rI;

            if (editing)
            {
                rI = new VienumuSaraksts(mProgram);
            }
            else
            {
                rI = new VienumaIzveide(mProgram);
            }
            Misc.RedirectWindow((Window)this, rI);
        }
    }
}
