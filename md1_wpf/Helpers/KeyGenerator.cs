﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.Helpers
{
    /// <summary>
    /// Klase ģenerē lietotnes izpildes laikā unikālus id, kurus izmanto, lai ģenerētu atslēgas vienumiem
    /// </summary>
    public static class KeyGenerator
    {
        private static uint id = 0;

        public static uint UseKey()
        {
            return id++;
        }

        public static uint SetStartValue
        {
            set
            {
                id = value;
            }
        }

    }
}
