﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.Helpers
{
    /// <summary>
    /// Date klase, kas tika prasīta uzdevumā
    /// Klase glabā Date daļu no DateTime klases objektiem
    /// </summary>
    public class Date
    {
        private DateTime mDate;

        public Date()
        {
            mDate = DateTime.Now.Date;
        }

        public Date(DateTime t)
        {
            mDate = t.Date;
        }

        public DateTime Value
        {
            get
            {
                return mDate;
            }
            set
            {
                mDate = value.Date;
            }
        }



    }
}
