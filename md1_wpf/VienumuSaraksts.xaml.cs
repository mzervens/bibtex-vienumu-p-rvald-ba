﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using md1.BibTex;

namespace md1_wpf
{
    /// <summary>
    /// Interaction logic for VienumuSaraksts.xaml
    /// Klase, kas nodrošina lietotnē reģistrēto vienumu apskati.
    /// Tiek ģenerēts saraksts, kur katra saraksta rinda reprezentē vienu vienumu, katrs vienums tiek parādīts formā
    /// <Raksta veids>: <Raksta nosaukums> <Skatīt sīkāk>
    /// Kur skatīt sīkāk ļauj detalizēti apskatīt attiecīgo vienumu (md1_wpf.SkatitVienumu)
    /// </summary>
    public partial class VienumuSaraksts : Window
    {
        private Program mProgram;
        private StackPanel vienumuSaraksts;
        private Label emptyMessage;

        public VienumuSaraksts(Program p)
        {
            mProgram = p;
            InitializeComponent();

            vienumuSaraksts = (StackPanel)this.FindName("list");
            emptyMessage = (Label)this.FindName("empty");

            if(!mProgram.isEmpty())
            {
                emptyMessage.Visibility = System.Windows.Visibility.Collapsed;
                GenerateList();
            }
        }

        private void GenerateList()
        {
            for(int i = 0; i < mProgram.mGramatas.Count; i++)
            {
                string nameKey = "book_" + i.ToString();
                string typeLabelVal = "Grāmata: ";
                string title = mProgram.mGramatas[i].nosaukums;
                TextBlock container = createRow(nameKey, typeLabelVal, title);

                vienumuSaraksts.Children.Add(container);
            }

            for (int i = 0; i < mProgram.mRaksti.Count; i++)
            {
                string nameKey = "article_" + i.ToString();
                string typeLabelVal = "Raksts: ";
                string title = mProgram.mRaksti[i].nosaukums;
                TextBlock container = createRow(nameKey, typeLabelVal, title);

                vienumuSaraksts.Children.Add(container);
            }

            for(int i = 0; i < mProgram.mNoslegumaDarbi.Count; i++)
            {
                string nameKey = "";
                string typeLabelVal = "";
                if(mProgram.mNoslegumaDarbi[i].DarbaVeids == md1.BibTex.NoslegumaDarbaTips.MAGISTRA_DARBS)
                {
                    nameKey = "mastersthesis_" + i.ToString(); 
                    typeLabelVal = "Maģistra darbs: ";
                }
                else
                {
                    nameKey = "phdthesis_" + i.ToString();
                    typeLabelVal = "Doktora disertācija: ";
                }
                string title = mProgram.mNoslegumaDarbi[i].nosaukums;
                TextBlock container = createRow(nameKey, typeLabelVal, title);

                vienumuSaraksts.Children.Add(container);
            }


        }

        private TextBlock createRow(string id, string typeVal, string titleVal)
        {
            Run title = new Run(Misc.TruncateIfTooLong(titleVal, 40));
            Run typeLabelVal = new Run(typeVal);



            Span typeLabel = new Span(typeLabelVal);
            Span titleLabel = new Span(title);
            Button viewBtn = new Button();

            viewBtn.Name = id;
            viewBtn.AddHandler(Button.ClickEvent, new RoutedEventHandler(ViewElement));
            viewBtn.Content = "Skatīt";
            viewBtn.FontSize = 12;
            viewBtn.Margin = new Thickness(10, 3, 0, 0);
            viewBtn.Height = 19;
            viewBtn.Width = 48;

            typeLabel.FontSize = titleLabel.FontSize = 16;
            titleLabel.FontWeight = FontWeights.Bold;

            TextBlock container = new TextBlock();
            container.Inlines.Add(typeLabel);
            container.Inlines.Add(titleLabel);
            container.Inlines.Add(viewBtn);

            return container;
        }

        private void ViewElement(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(sender.ToString() + " : " + e.ToString());
            Button clickedBtn = (Button)sender;
            string elementId = clickedBtn.Name;
            string[] parts = elementId.Split('_');

            EntryTypes type = TypeValueToEntryType.literalToEnum[parts[0]];
            int index = int.Parse(parts[1]);

            Window skatitVienumu = new SkatitVienumu(mProgram, index, type);
            Misc.RedirectWindow((Window)this, skatitVienumu);
        }

        private void GoBack(object sender, RoutedEventArgs e)
        {
            Window mW = new MainWindow(mProgram);
            Misc.RedirectWindow((Window)this, mW);
        }

    }
}
