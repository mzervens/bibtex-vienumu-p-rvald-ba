﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace md1.BibTex.IO
{
    /// <summary>
    /// Vispārīga vienuma klase, kas glabā vienuma datus importējot vienumu no teksta faila.
    /// Šīs klases objektus (ar aizpildītiem datiem) var tālāk padot konkrētu vienumu klasēm, lai iegūtu attiecīgā tipa objektus.
    /// Klases galvenā funkcionalitāte atrodas metodē ProcessData(), kas veic lauka mRawData parsēšanu
    /// Lai veiksmīgi noritētu parsēšana, laukā mRawData ir jāatrodas korekta BibTeX formāta vienums, piemēram
    /// @BOOK{RattzFreeman2010,
    ///     title = {Pro LINQ  Language Integrated Query in C 2010},
    ///     publisher = {Apress},
    ///     year = {2010},
    ///     author = {Joeseph Rattz, Adam Freeman},
    ///     address = {New York},
    ///     owner = {User},
    ///     timestamp = {2015.09.25}
    ///    }
    /// </summary>
    public class ImportEntity
    {
        private string mRawData = "";
        public string mType = "";
        public EntryTypes mTypeId = EntryTypes.UNKNOWN;
        public string mKey = "";
        public Dictionary<string, string> mEntries = new Dictionary<string, string>();


        public ImportEntity()
        {
            
        }

        public ImportEntity(string data)
        {
            mRawData = data;
        }

        private void UpdateTypeId()
        {
            string typeLower = mType.ToLower();
            if(TypeValueToEntryType.literalToEnum.ContainsKey(typeLower))
            {
                mTypeId = TypeValueToEntryType.literalToEnum[typeLower];
            }
            else
            {
                mTypeId = EntryTypes.UNKNOWN;
            }

        }

        public void ProcessData()
        {
            int typeEndIdx = mRawData.IndexOf('{');

            if(typeEndIdx == -1)
            {
                return;
            }

            string typePart = mRawData.Substring(0, typeEndIdx);
            string dataPart = mRawData.Substring(typeEndIdx);

            typePart = typePart.Trim();
            if(typePart[0] != '@')
            {
                return;
            }

            typePart = typePart.Substring(1);
            mType = typePart;

            UpdateTypeId();

            string[] explodedData = dataPart.Split(',');
            if(explodedData.Length == 0)
            {
                return;
            }


            string unfinishedEntry = "";
            for (int i = 0; i < explodedData.Length; i++)
            {

                if(i == 0)
                {
                    char[] trimChars = {'{'};
                    explodedData[i] = explodedData[i].Trim().Trim(trimChars);
                    mKey = explodedData[i];
                }
                else
                {
                    explodedData[i] = explodedData[i].Trim();

                    if (!explodedData[i].Contains('=') && unfinishedEntry == "")
                    {
                        continue;
                    }

                    if (unfinishedEntry == "" && explodedData[i][explodedData[i].Length - 1] == '}')
                    {
                        string[] explodedEntry = explodedData[i].Split('=');
                        if (explodedEntry.Length != 2)
                        {
                            continue;
                        }

                        char[] leftTrimChars = { '{' }, rightTrimChars = { '}' };

                        string entryKey = explodedEntry[0].Trim().ToLower();
                        string entryVal = explodedEntry[1].Trim().TrimStart(leftTrimChars).TrimEnd(rightTrimChars).TrimEnd().TrimEnd(rightTrimChars);

                        mEntries.Add(entryKey, entryVal);
                        continue;
                    }
                    else if(unfinishedEntry == "" && explodedData[i].Contains('=') && explodedData[i][explodedData[i].Length - 1] != '}')
                    {
                        unfinishedEntry = explodedData[i];
                        continue;
                    }
                    else if(unfinishedEntry != "" && explodedData[i][explodedData[i].Length - 1] == '}')
                    {
                        string entryKeyVal = unfinishedEntry + ',' + explodedData[i];

                        string[] explodedEntry = entryKeyVal.Split('=');
                        if (explodedEntry.Length != 2)
                        {
                            unfinishedEntry = "";
                            continue;
                        }

                        char[] leftTrimChars = { '{' }, rightTrimChars = { '}' };

                        string entryKey = explodedEntry[0].Trim().ToLower();
                        string entryVal = explodedEntry[1].Trim().TrimStart(leftTrimChars).TrimEnd(rightTrimChars).TrimEnd().TrimEnd(rightTrimChars);

                        mEntries.Add(entryKey, entryVal);
                        unfinishedEntry = "";
                        continue;
                    }
                    else if(unfinishedEntry != "" && explodedData[i][explodedData[i].Length - 1] != '}')
                    {
                        unfinishedEntry += ',' + explodedData[i];
                    }


                }
            }

        }


        public void print()
        {
            Console.WriteLine("=== Entity ===");
            Console.WriteLine("type :" + mType);
            Console.WriteLine("typeId :" + mTypeId);
            Console.WriteLine("key :" + mKey);

            foreach(KeyValuePair<string, string> entry in mEntries)
            {
                Console.WriteLine("entry :" + entry.Key + " " + entry.Value);
            }

        }



    }
}
