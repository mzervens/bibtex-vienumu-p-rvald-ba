﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace md1.BibTex.IO
{
    /// <summary>
    /// Klase nodrošina importējamā faila apstrādi pa entītijām
    /// Attiecīgi fails tiek sadalīts pa vienumiem pēc simboliem '@',
    /// katrs faila elements tālāk tiek parsēts iekš md1.BibTex.IO.ImportEntity
    /// Importa rezultātā tiek atgriezts saraksts ar ImportEntity objektiem, kam tika atpazīts tips
    /// Šo sarakstu tālāk var izmantot, lai veidotu konkrētu klašu objektus.
    /// </summary>
    public static class Import
    {

        public static List<ImportEntity> FromTextFile(string filePath)
        {
            List<ImportEntity> output = new List<ImportEntity>();
            string wholeFile = "";

            if(!File.Exists(filePath))
            {
                return output;
            }

            StreamReader sr = new StreamReader(filePath);
            wholeFile = sr.ReadToEnd();

            sr.Close();

            string[] entitiesInFile = wholeFile.Split('@');

            foreach(string entity in entitiesInFile)
            {
                if(entity.Length <= 1)
                {
                    continue;
                }

                ImportEntity e = new ImportEntity('@' + entity);
                e.ProcessData();

                if(e.mTypeId != EntryTypes.UNKNOWN)
                {
                    output.Add(e);
                }
            }

            return output;
        }


    }
}
