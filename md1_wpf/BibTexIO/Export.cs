﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace md1.BibTex.IO
{
    /// <summary>
    /// Klase ļauj saglabāt padoto teksta virkni failā
    /// </summary>
    public static class Export
    {
        public static void deleteFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public static void ToTextFile(string filePath, string data, bool clearContents = false)
        {
            if(clearContents)
            {
                deleteFile(filePath);
            }

            StreamWriter sw = File.AppendText(filePath);
            sw.WriteLine(data);

            sw.Close();
        }

    }
}
