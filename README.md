#BibTeX vienumu pārvaldības lietotne

* Lietotne tika izstrādāta kā mājas darbs LU .NET kursā.
* Šie ir galvenie resursi, kas tika izmantoti, lai realizētu šo darbu:
* [set / get properties iekš C#](https://msdn.microsoft.com/en-us/library/w86s7x04.aspx)
* [C# removing strings from end of string](http://stackoverflow.com/questions/4101539/c-sharp-removing-strings-from-end-of-string)
* [BibTeX apraksts](http://maverick.inria.fr/~Xavier.Decoret/resources/xdkbibtex/bibtex_summary.html)
* [Failu rakstīšana](https://msdn.microsoft.com/en-us/library/system.io.file.appendtext(v=vs.110).aspx)
* [WPF combobox vērtību apstrāde](http://stackoverflow.com/questions/4351603/get-selected-value-from-combo-box-in-c-sharp-wpf)
* [Nemeric input iekš textbox (WPF)](http://stackoverflow.com/questions/1268552/how-do-i-get-a-textbox-to-only-accept-numeric-input-in-wpf)
* [Event handler pievienošana kodā ģenerētiem elementiem](http://stackoverflow.com/questions/4663372/how-to-add-event-handler-programmatically-in-wpf-like-one-can-do-in-winform)
* [File browser menu izveide](http://stackoverflow.com/questions/10315188/open-file-dialog-and-select-a-file-using-wpf-controls-and-c-sharp)


#Komentāri

* Darbā tika lietoti ļoti vispārīgi komentāri, tika sniegts vienkāršs katras klases apraksts (ko tā dara augstā līmenī).
* Autors izvēlējās nekomentēt katru metodi, jo lietotnes problēmas apgabals ir samērā triviāls (attiecīgie algoritmi / loģika ir vienkārša). Papildus, izveidotā lietotne sanāca relatīvi apjomīga ņemot vērā, ka šis  ir viens mājas darbs 2KP kursa, lielo metožu skaitu katru komentēt aizņemtu ļoti daudz laika.


#Autors

* Matīss Zērvēns